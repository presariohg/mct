clear all
close all

img = imread("lena512.bmp");
img = int16(img); % Cast to int16


figure % A1-1a figure
image(img)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])


figure % A1-1b figure
image(rot90(img))
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])


figure % A1-1c figure
inverted_img = 255 - img;
image(inverted_img)
colormap(gray(256)) % 8 bit grayscale, monochrome
daspect([1 1 1])


figure % A1-1d figure
reduced_img = img; % Make a copy
reduced_img(reduced_img <= 50) = 0; % Reduce all <= 50 elems to 0
reduced_img(reduced_img >= 200) = 255; % "Reduce" all >= 200 elems to 255
image(reduced_img)
colormap(gray(256))
daspect([1 1 1])


figure % A1-1e normal figure
[X_MAX, Y_MAX] = size(img);
chunky_img = img; % Make a copy
STEP = 64;
for y = 1 : STEP : Y_MAX
	next_y = y + STEP - 1;
	for x = 1 : STEP : X_MAX
		next_x = x + STEP - 1;
		chunky_img(x : next_x, y : next_y) = rot90(chunky_img(x : next_x, y : next_y));
	end
end

image(chunky_img)
colormap(gray(256))
daspect([1 1 1])


figure % A1-1e advanced figure
[X_MAX, Y_MAX] = size(img);
chunky_img = img; % Make a copy
STEP = 64;

column_is_even = false % starts at 1
row_is_even = false % starts at 1

for y = 1 : STEP : Y_MAX
	next_y = y + STEP - 1;
	for x = 1 : STEP : X_MAX
		next_x = x + STEP - 1;
		if not(xor(column_is_even, row_is_even)) % if both are even or odd
			chunky_img(x : next_x, y : next_y) = rot90(chunky_img(x : next_x, y : next_y));
		end

		column_is_even = not(column_is_even);
	end
	row_is_even = not(row_is_even);
end

image(chunky_img)
colormap(gray(256))
daspect([1 1 1])


figure % A1-1f figure
histogram(img)


% A1-1g 
disp(entropy(imread("lena512.bmp")) + " bit/pixel")

