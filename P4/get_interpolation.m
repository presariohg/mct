function [best_block, best_weight] = get_interpolation(pred_1, pred_2, orig)

best_mse = 9999999.9; % chose an abitrary large number as the first min value
best_weight_2 = 0;
result = orig;

PIXEL_COUNT = numel(orig);

for weight_1 = 0 : 0.1 : 1
	weight_2 = 1 - weight_1;

	candidate = weight_1 * pred_1 + weight_2 * pred_2;

	% find the block with smallest mse
	diff_block = int16(orig) - int16(candidate);
	mse = sum(diff_block.^2, 'all') / PIXEL_COUNT;
	if mse < best_mse
		best_mse = mse;
		result = candidate;
		best_weight_2 = weight_2;
	end
end

best_block = result;
best_weight = best_weight_2;