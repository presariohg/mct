function yext = mirroredframe(y, range)

% damn you, arrays-start-at-1!!
[orig_y_max, orig_x_max] = size(y);

padded_x_max = orig_x_max + 2*range;
padded_y_max = orig_y_max + 2*range;

padded_vertical = zeros(padded_y_max, orig_x_max); % pad upper & lower first
padded_horizontal = zeros(padded_y_max, padded_x_max); % then pad left & right

orig_x_start = range + 1;
orig_x_end = orig_x_start + orig_x_max - 1;

orig_y_start = range + 1;
orig_y_end = orig_y_start + orig_y_max - 1;

% put the origin img in the vertical center of the vertically padded img
padded_vertical(orig_y_start : orig_y_end, 1 : orig_x_max) = y; 

for col = 1 : orig_x_max
	flipped_col = flip(y(:, col));

	% padding upper
	padded_vertical(1 : range, col) = flipped_col(orig_y_max - range + 1 : orig_y_max);

	% padding lower
	padded_vertical(orig_y_end + 1 : padded_y_max, col) = flipped_col(1 : range);
end

% put the vertically padded img in the horizontal center of the padded img
padded_horizontal(1 : padded_y_max, orig_x_start : orig_x_end) = padded_vertical;

for row = 1 : padded_y_max
	flipped_row = flip(padded_vertical(row, :));

	% padding left
	padded_horizontal(row, 1 : range) = flipped_row(orig_x_max - range + 1 : orig_x_max);

	% padding right
	padded_horizontal(row, orig_x_end + 1 : padded_x_max) = flipped_row(1 : range);
end

yext = padded_horizontal;

